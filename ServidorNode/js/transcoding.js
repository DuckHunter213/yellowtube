const spawn = require('child_process').spawn;
//funcion para mandar a llamar el transcoding
function transcodingVideo(path, video, quality) {
    const p = new Promise((resolve, reject) => {
        //Aqui es el tipo de formato que va a aceptar el servidor.
        const ffmpeg = spawn('ffmpeg', ['-i', `${path}/${video}`, '-codec:v', 'libx264', '-profile:v', 'main', '-preset', 'slow', '-b:v', '400k', '-maxrate', '400k', '-bufsize', '800k', '-vf', `scale=-2:${quality}`, '-threads', '0', '-b:a', '128k', `${path}/transcoded/${video}_${quality}.mp4`]);
        //const ffmpeg = spawn('ffmpeg', ['-i', `${path}/${video}`, '-codec:v', 'libtheora','-b:v', '400k', `${path}/transcoded/${video}_${quality}.ogg`]);
        ffmpeg.stderr.on('data', (data) => {
            console.log(`${data}`);
        });
        ffmpeg.on('close', (code) => {
            resolve();
        });
    });
    return p;
}
