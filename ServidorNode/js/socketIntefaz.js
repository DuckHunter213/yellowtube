var $ = require('../YellowTube/js/jquery-3.2.1.js')

$(document).on("DOMContentLoaded", function () {
    // Initialize instances:
    var socket = io.connect();
    var siofu = new SocketIOFileUpload(socket);

    // Configure the three ways that SocketIOFileUpload can read files:
    $("#upload_btn").on("click", siofu.prompt, false);
    siofu.listenOnInput($("#upload_btn"));
    siofu.listenOnDrop($("#file_drop"));

    // Do something on upload progress:
    siofu.addEventListener("progress", function (event) {
        var percent = event.bytesLoaded / event.file.size * 100;
        console.log("File is", percent.toFixed(2), "percent loaded");
    });

    // Do something when a file is uploaded:
    siofu.addEventListener("complete", function (event) {
        console.log(event.success);
        console.log(event.file);
    });

}, false);