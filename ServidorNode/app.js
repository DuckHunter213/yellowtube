/* Ejemplo de como crear un servidor en nodejs */
var http = require('http');
var server = http.createServer(function (request, response) {
    response.writeHead(200, {
        'Content-Type': 'html/plain'
    });
    response.write('<h1>Front End Labs</h1>');
    response.end();
});
server.listen(80);
console.log('Server Start : D');