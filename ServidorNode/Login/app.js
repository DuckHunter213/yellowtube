var express = require('express');
var io = require('socket.io');
var cookie = require('cookie'); //Para parsear las cookies que vengan en la request
var loginController = require('./loginController.js');
//Server---------------------------------------------
var PORT = 8888;
var app = express();
var session = new Object(); //En esta variable se almacenarán los tokens de sesión
//Añadimos estas funciones a "app" para recibir los valores enviados por POST
app.use(express.bodyParser());
app.use(express.methodOverride());
//Levantamos el servidor
var server = app.listen(PORT, function () {
    loginController.controller(app, session); //Incluimos los controladores para el inicio de sesión
    console.log("Listen on port " + PORT);
});
app.use("/js", express.static(__dirname + '/static/js')); //Hacemos público el directorio que contiene el Javascript de cliente
//IO---------------------------------------------------
var io = io.listen(server); //Iniciamos Socket.IO
io.configure(function () {
    io.set('authorization', function (handshakeData, callback) {
        var cookies = cookie.parse(handshakeData.headers.cookie); //Parseamos las cookies enviadas en la request
        var sessionId = cookies['sessionId']; //Obtenemos el token de sesión
        if (session[sessionId]) {
            callback(null, true); //Si el token de sesión enviado en la cookie está almacenado, permitimos la conexión con el socket
        }
        else {
            return callback(null, false); //Denegamos la conexión
        }
    });
});
var socket = io.sockets.on('connection', function (socket) {
    console.log('Client connected to socket'); //Se establece la conexión con el socket sólo si el token de sesión es correcto
});