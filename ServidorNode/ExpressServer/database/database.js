const mysql = require('mysql');
const colors = require('colors/safe');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: ""
});

con.connect(function (err) {
    if (err){
        console.log(colors.red(err));
    }else{
        console.log(colors.green("Database connected!"));
    }
});

module.exports = mysql;