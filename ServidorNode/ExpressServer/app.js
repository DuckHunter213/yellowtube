const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const colors = require('colors/safe');

const app = express();
const bd = require('./database/database.js');
const routes = require('./routes/index.js');

// db settings



// settings
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.set('port', process.env.PORT || 80);

// middlewares
app.use((req, res, next) => {
	console.log(`${req.url} - ${req.method}`);
	next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.use(routes);

// satic files
app.use(express.static(path.join(__dirname, 'public')));

// bootstraping the app
app.listen(80, () => console.log(colors.green('Server listening on port 80')));