var colors = require('colors/safe');
var transcoding = require('./js/transcoding');

//Socket de que sube archivos
//#region Declaracion de socket y express
var SocketIOFileUpload = require('socketio-file-upload'),
    socketio = require('socket.io'),
    express = require('express');
var port = 80
// Para hacer el server de express
var app = express()
    .use(SocketIOFileUpload.router)
    .use(express.static(__dirname))
    .listen(port);
//#endregion
console.log(colors.green("El servidor esta corriendo en el puerto " + port));

var io = socketio.listen(app);
io.sockets.on("connection", function (socket) {

    console.log(colors.blue("Nuevo usuario conectado: "+socket.client))
    // Make an instance of SocketIOFileUpload and listen on this socket:
    var uploader = new SocketIOFileUpload();
    uploader.dir = __dirname; //+ "/srv/uploads/";
    uploader.listen(socket);

    // Do something when a file is saved:
    uploader.on("saved", function (event) {
        console.log("File saved in: " + __dirname + "\\" + event.file.name);
        console.log(event.file);
        transcoding.resizeVideo(__dirname, event.file.name, 360);
    });

    // Error handler:
    uploader.on("error", function (event) {
        console.log("Error from uploader", event);
    });
});

